
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/seq_file.h>
#include <linux/cdev.h>
#include <linux/list.h>
#include <linux/mutex.h>
#include <linux/poll.h>
#include <linux/ioctl.h>
#include <linux/signal.h>

//#include <asm/system.h>		/* cli(), *_flags */
//#include <asm/semaphore.h>
#include <asm/uaccess.h>	/* copy_*_user */

#include "gnusmaser.h"		/* local definitions */

MODULE_AUTHOR("Vitaliy Panchenko");
MODULE_LICENSE("Dual BSD/GPL");

int gnus_major =   0;
int gnus_minor =   0;
module_param(gnus_major, int, S_IRUGO);
module_param(gnus_minor, int, S_IRUGO);

struct gnus_device *gnus_dev = NULL;
static int gnus_fasync(int fd, struct file *filp, int mode);

/////////////////////////////////////////////////////////////////////////////////
/*
 * Open and close
 */
int gnus_open(struct inode *inode, struct file *filp)
{
	struct gnus_device *dev; /* device information */

	/*  Find the device */
	dev = container_of(inode->i_cdev, struct gnus_device, cdev);

		/* now trim to 0 the length of the device if open was write-only */
	if ( (filp->f_flags & O_ACCMODE) == O_WRONLY) {
		if (down_interruptible (&dev->sem))
			return -ERESTARTSYS;
		//scullp_trim(dev); /* ignore errors */
		up (&dev->sem);
	}

	/* and use filp->private_data to point to the device data */
	filp->private_data = dev;
	return 0;          /* success */
}

int gnus_release(struct inode *inode, struct file *filp)
{
	/* remove this filp from the asynchronously notified filp's */
	gnus_fasync(-1, filp, 0);
	return 0;
}

/*
 * Data management: read and write
 */
ssize_t gnus_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	struct gnus_device *dev = filp->private_data;
	struct gnus_record *rec = dev->last_rec;
	int result;

	/* lock */
	if (down_interruptible(&dev->sem))
		return -ERESTARTSYS;

	if (!rec)
		return 0;

	if (copy_to_user(buf, rec->str, rec->str_len)) {
		up (&dev->sem);
		return -EFAULT;
	}

	/* unlock */
	up(&dev->sem);
	return rec->str_len;
}

ssize_t gnus_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)
{
	struct gnus_device *dev = filp->private_data;
	struct gnus_record *rec = NULL;
	int result;

	/* lock */
	if (down_interruptible(&dev->sem))
		return -ERESTARTSYS;

	rec = kmem_cache_alloc(dev->cache_rec, GFP_ATOMIC);
	if (!rec) {
		printk("kmem_cache_alloc ERR, dev %d\n", dev->numb);
		return -ENOMEM;
	}
	memset(rec, 0, sizeof(struct gnus_record));
	rec->str = kmalloc(count+1, GFP_KERNEL);
	if (!rec->str) {
		printk("kmalloc ERR, dev %d\n", dev->numb);
		result = -ENOMEM;
		goto bad;
	}
	if (copy_from_user(rec->str, buf, count)) {
		printk("copy_from_user ERR, count %d, dev %d\n", count, dev->numb);
		result = -EFAULT;
		goto bad;
	}
	rec->str[count] = 0;
	rec->str_len = count;
	list_add(&rec->list, &dev->str_head);
	dev->size++;
	dev->last_rec = rec;

	/* unlock */
	up(&dev->sem);

	/* and signal asynchronous readers, explained late in chapter 5 */
	if (dev->async_queue)
		kill_fasync(&dev->async_queue, SIGIO, POLL_IN);
	return count;

bad:
	if (dev->cache_rec) {
		kmem_cache_free(dev->cache_rec, rec);
		if (rec->str)
			kfree(rec->str);
	}
	up(&dev->sem);
	return result;
}


/*
 * The ioctl() implementation
 */
//int gnus_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
static long gnus_unlocked_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	return 0;
}

static int gnus_fasync(int fd, struct file *filp, int mode)
{
	struct gnus_device *dev = filp->private_data;
	return fasync_helper(fd, filp, mode, &dev->async_queue);
	return 0;
}

struct file_operations gnus_fops = {
	.owner 			= THIS_MODULE,
	.llseek 		= NULL,
	.read 			= gnus_read,
	.write 			= gnus_write,
	.unlocked_ioctl = gnus_unlocked_ioctl,
	.open 			= gnus_open,
	.release 		= gnus_release,
	.fasync			= gnus_fasync,
};

void __exit gnus_cleanup_module(void)
{
	int i;
	dev_t devno = MKDEV(gnus_major, gnus_minor);
	struct gnus_record *rec, *rec_tmp;

	printk("gnus: %s enter\n", __func__);

	/* Get rid of our char dev entries */
	if (gnus_dev) {
		for (i = 0; i < GNUS_NR_DEVICES; i++) {
			struct gnus_device *my_dev = &gnus_dev[i];
			cdev_del(&my_dev->cdev);
			if (my_dev->ref_str)
				kfree(my_dev->ref_str);

			/* delete saved strings */
			list_for_each_entry_safe(rec, rec_tmp, &my_dev->str_head, list) {
				if (rec->str)
					kfree(rec->str);
				list_del(&rec->list);
				kmem_cache_free(my_dev->cache_rec, rec);
			}

			/* delete data_cache */
			if (my_dev->cache_rec)
				kmem_cache_destroy(my_dev->cache_rec);
		}
		kfree(gnus_dev);
	}

	/* cleanup_module is never called if registering failed */
	unregister_chrdev_region(devno, GNUS_NR_DEVICES);

	printk("gnus: %s OK\n", __func__);
}

int __init gnus_init_module(void)
{
	int result, i;
	dev_t dev = 0;

	printk("gnus: %s enter\n", __func__);

	result = alloc_chrdev_region(&dev, gnus_minor, GNUS_NR_DEVICES, "Device_");
	gnus_major = MAJOR(dev);
	if (result < 0) {
		printk(KERN_WARNING "gnus: can't get major %d\n", gnus_major);
		return result;
	}

	gnus_dev = kmalloc(GNUS_NR_DEVICES * sizeof(struct gnus_device), GFP_KERNEL);
	if (!gnus_dev) {
		result = -ENOMEM;
		goto fail;
	}
	memset(gnus_dev, 0, GNUS_NR_DEVICES * sizeof(struct gnus_device));

	for (i=0; i < GNUS_NR_DEVICES; i++) {
		struct gnus_device *my_dev = &gnus_dev[i];
		struct cdev *cdev = &my_dev->cdev;

		sema_init(&my_dev->sem, 1);
		INIT_LIST_HEAD(&my_dev->str_head);

		cdev_init(&my_dev->cdev, &gnus_fops);
		cdev->owner = THIS_MODULE;
		cdev->ops = &gnus_fops;
		dev = MKDEV(gnus_major, gnus_minor + i);
		result = cdev_add(cdev, dev, 1);
		if (result < 0) {
			printk(KERN_NOTICE "Error %d adding gnus %d", result, i);
			goto fail;
		}

		my_dev->cache_rec = kmem_cache_create("data_cache",
								sizeof(struct gnus_record),
								0,
								SLAB_HWCACHE_ALIGN, NULL);
		if (!my_dev->cache_rec) {
			printk(KERN_NOTICE "kmem_cache_create ERR, gnus %d", i);
			goto fail;
		}

		my_dev->ref_str = NULL;
		my_dev->last_rec = NULL;
		my_dev->size = 0;
		my_dev->numb = i;
	}

	printk("gnus: %s OK\n", __func__);
	return 0; /* succeed */

fail:
	printk("gnus: %s error\n", __func__);
	gnus_cleanup_module();
	return result;
}

module_init(gnus_init_module);
module_exit(gnus_cleanup_module);
