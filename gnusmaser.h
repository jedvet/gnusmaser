
#ifndef _GNUS_DEF
#define _GNUS_DEF

// $(awk "\$2==\"Device_\" {print \$1}" /proc/devices)

/////////////////////////////////////////////////////////////////////////////////
#define GNUS_NR_DEVICES		2

/////////////////////////////////////////////////////////////////////////////////
struct gnus_record {
	struct list_head list;
	char *str;
	int str_len;
};

struct gnus_device {
	struct list_head str_head;
	char *ref_str;
	struct gnus_record *last_rec;
	unsigned long size;       	/* amount of strings stored here */
	struct semaphore sem;  		/* mutual exclusion semaphore     */
	struct fasync_struct *async_queue; /* asynchronous readers */
	struct kmem_cache *cache_rec; /* memcache for the struct gnus_data */
	struct cdev cdev;	  		/* Char device structure		*/
	int numb;
};

#endif
